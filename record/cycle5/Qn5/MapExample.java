import java.util.HashMap;
import java.util.Map;

public class MapExample {
    public static void main(String[] args) {
        // Creating a HashMap
        Map<String, Integer> hashMap = new HashMap<>();

        // Adding key-value pairs to the map
        hashMap.put("Apple", 10);
        hashMap.put("Banana", 20);
        hashMap.put("Cherry", 30);
        hashMap.put("Date", 40);
        hashMap.put("Fig", 50);

        // Displaying the map
        System.out.println("HashMap elements: " + hashMap);

        // Demonstration of Map interface methods
        System.out.println("\nMap Interface Methods:");

        // 1. Using get() method
        System.out.println("Value associated with key 'Banana': " + hashMap.get("Banana"));

        // 2. Using containsKey() method
        System.out.println("Does map contain key 'Cherry'? " + hashMap.containsKey("Cherry"));

        // 3. Using containsValue() method
        System.out.println("Does map contain value '60'? " + hashMap.containsValue(60));

        // 4. Using size() method
        System.out.println("Size of the map: " + hashMap.size());

        // 5. Using remove() method
        hashMap.remove("Date");
        System.out.println("HashMap after removing key 'Date': " + hashMap);

        // 6. Using keySet() method
        System.out.println("Keys in the map: " + hashMap.keySet());

        // 7. Using values() method
        System.out.println("Values in the map: " + hashMap.values());

        // 8. Using entrySet() method
        System.out.println("Key-Value pairs in the map: " + hashMap.entrySet());
    }
}

