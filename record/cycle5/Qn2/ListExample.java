import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class ListExample {
    public static void main(String[] args) {
        // Creating an ArrayList
        List<String> arrayList = new ArrayList<>();

        // Adding elements to the ArrayList
        arrayList.add("Apple");
        arrayList.add("Banana");
        arrayList.add("Cherry");
        arrayList.add("Date");
        arrayList.add("Fig");

        // Printing the ArrayList
        System.out.println("ArrayList elements:");
        System.out.println(arrayList);

        // Demonstration of List interface methods
        System.out.println("\nList Interface Methods:");

        // 1. Using size() method
        System.out.println("Size of the ArrayList: " + arrayList.size());

        // 2. Using get() method
        System.out.println("Element at index 2: " + arrayList.get(2));

        // 3. Using set() method
        arrayList.set(1, "Blueberry");
        System.out.println("ArrayList after setting element at index 1 to 'Blueberry':");
        System.out.println(arrayList);

        // 4. Using remove() method
        arrayList.remove(3);
        System.out.println("ArrayList after removing element at index 3:");
        System.out.println(arrayList);

        // 5. Using contains() method
        System.out.println("Does ArrayList contain 'Apple'? " + arrayList.contains("Apple"));
        System.out.println("Does ArrayList contain 'Grape'? " + arrayList.contains("Grape"));

        // Printing elements using ListIterator
        System.out.println("\nPrinting elements using ListIterator:");
        ListIterator<String> listIterator = arrayList.listIterator();
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next());
        }
    }
}

