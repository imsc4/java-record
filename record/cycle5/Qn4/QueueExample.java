import java.util.LinkedList;
import java.util.Queue;

public class QueueExample {
    public static void main(String[] args) {
        // Creating a Queue using LinkedList
        Queue<String> queue = new LinkedList<>();

        // Adding elements to the queue
        queue.add("Apple");
        queue.add("Banana");
        queue.add("Cherry");
        queue.add("Date");
        queue.add("Fig");

        // Displaying the queue
        System.out.println("Queue elements: " + queue);

        // Demonstration of Queue interface methods
        System.out.println("\nQueue Interface Methods:");

        // 1. Using add() method
        queue.add("Grape");
        System.out.println("Queue after adding 'Grape': " + queue);

        // 2. Using offer() method
        boolean isOffered = queue.offer("Honeydew");
        System.out.println("Is 'Honeydew' offered successfully? " + isOffered);
        System.out.println("Queue after offering 'Honeydew': " + queue);

        // 3. Using remove() method
        String removedElement = queue.remove();
        System.out.println("Removed element from the queue: " + removedElement);
        System.out.println("Queue after removing an element: " + queue);

        // 4. Using poll() method
        String polledElement = queue.poll();
        System.out.println("Polled element from the queue: " + polledElement);
        System.out.println("Queue after polling an element: " + queue);

        // 5. Using peek() method
        String peekedElement = queue.peek();
        System.out.println("Peeked element from the queue: " + peekedElement);
        System.out.println("Queue after peeking an element: " + queue);

        // 6. Using element() method
        String firstElement = queue.element();
        System.out.println("First element of the queue: " + firstElement);
        System.out.println("Queue after accessing the first element: " + queue);

        // 7. Using size() method
        System.out.println("Size of the queue: " + queue.size());

        // 8. Using isEmpty() method
        System.out.println("Is queue empty? " + queue.isEmpty());
    }
}

