import java.util.Stack;

public class StackExample {
    public static void main(String[] args) {
        // Creating a stack
        Stack<Integer> stack = new Stack<>();

        // Pushing elements onto the stack
        stack.push(10);
        stack.push(20);
        stack.push(30);
        stack.push(40);
        stack.push(50);

        // Displaying the stack
        System.out.println("Stack elements: " + stack);

        // Popping elements from the stack
        System.out.println("Popped element: " + stack.pop());
        System.out.println("Popped element: " + stack.pop());

        // Displaying the stack after popping
        System.out.println("Stack elements after popping: " + stack);

        // Peeking at the top element of the stack
        System.out.println("Top element of the stack: " + stack.peek());

        // Checking if the stack is empty
        System.out.println("Is stack empty? " + stack.isEmpty());

        // Getting the size of the stack
        System.out.println("Size of the stack: " + stack.size());
    }
}

