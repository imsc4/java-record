import java.util.HashSet;
import java.util.Iterator;

public class CollectionDemo {
    public static void main(String[] args) {
        // Create a HashSet to demonstrate Collection methods
        HashSet<String> carSet1 = new HashSet<>();
        HashSet<String> carSet2 = new HashSet<>();

        // Adding elements to the HashSet
        carSet1.add("Toyota");
        carSet1.add("Honda");
        carSet1.add("BMW");
        carSet1.add("Ford");

        // Displaying elements in carSet1
        System.out.println("Elements in carSet1: " + carSet1);

        // Demonstrating remove() method
        carSet1.remove("BMW");
        System.out.println("After removal, carSet1: " + carSet1);

        // Demonstrating isEmpty() method
        System.out.println("Is carSet1 empty? " + carSet1.isEmpty());

        // Demonstrating elements in the HashSet
        System.out.println("Elements in carSet1:");
        Iterator<String> iterator1 = carSet1.iterator();
        while (iterator1.hasNext()) {
            System.out.println(iterator1.next());
        }

        // Example for contains method
        System.out.println("Does carSet1 contain 'Toyota'? " + carSet1.contains("Toyota"));

        // Demonstrating size() method
        System.out.println("Size of carSet1: " + carSet1.size());

        // Demonstrating clear() method
        carSet1.clear();
        System.out.println("After clearing, carSet1: " + carSet1);

        // Adding elements to carSet1 after clearing
        carSet1.add("Mercedes");
        carSet1.add("Audi");
        System.out.println("After adding, carSet1: " + carSet1);
    }
}

