import java.util.Scanner;
public class Matrix {
    private int rows;
    private int coloumns;
    private double data[][];
    private String name;
    public Matrix(int r , int c){
        this.rows=r;
        this.coloumns=c;
        this.data=new double[rows][coloumns];
        
    }
    public void SetElement(int row ,int coloumn ,double value){
        if(row>=0 && row<rows && coloumn>=0 && coloumn<coloumns){
            data[row][coloumn]=value;
            
        }
        else{
            System.out.println("Invalid Position");
        }
    }
    
    public double getElement(int row, int coloumn){
        if(row>=0 && row<rows && coloumn>=0 && coloumn<coloumns){
            return data[row][coloumn];
            
        }
        else{
            System.out.println("invalid position");
            return -1;
            
        }
    }
    public void display(){
        System.out.println("Matrix : ");
        for(int i=0;i<rows;i++){
            for(int j=0;j<coloumns;j++){
                System.out.print(data[i][j]+" ");
            }
            System.out.println();
        }
    }
    public Matrix add (Matrix mat1){
        if(this.rows!=mat1.rows || this.coloumns!=mat1.coloumns){
            System.out.println("This Matrices cannot be added");
            return null;
        }
        else{
            Matrix sum = new Matrix (this.rows,this.coloumns);
            for(int i=0;i<this.rows;i++){
                for(int j=0;j<this.coloumns;j++){
                    double value=this.getElement(i,j)+mat1.getElement(i,j);
                    sum.SetElement(i,j, value);
                    
                }
            }
             return sum;   
            }
        }
    public Matrix sub (Matrix mat2){
        if(this.rows!=mat2.rows || this.coloumns!=mat2.coloumns){
            System.out.println("This matrices cannot be substracted");
            return null;
        }
        else{
            Matrix diff= new Matrix(this.rows,this.coloumns);
            for(int i=0;i<this.rows;i++){
                for(int j=0;j<this.coloumns;j++){
                    double value=this.getElement(i,j)-mat2.getElement(i,j);
                    diff.SetElement(i,j, value);
                }
            }
            return diff;
        }
    }
    public Matrix mul (Matrix mat3){
        if(this.coloumns!=mat3.rows){
            System.out.println("This matrices cannot be multiplied");
            return null;
        }
        else{
            Matrix prod=new Matrix (this.rows,this.coloumns);
            for(int i=0;i<this.rows;i++){
                for(int j=0;j<this.coloumns;j++){
                    double value=0;
                    for(int k=0;k<this.coloumns;k++){
                        value+=this.getElement(i,k)*mat3.getElement(k,j);
                    }
                    prod.SetElement(i,j, value);
                }
            }
            return prod;
        }
    }
    public Matrix transpose(){
        Matrix transpose=new Matrix(this.coloumns,this.rows);
        for(int i=0;i<this.rows;i++){
            for(int j=0;j<this.coloumns;j++){
                transpose.SetElement(j,i,this.getElement(i,j));
            }
        }
        return transpose;
    }
    public String toString(){
        StringBuilder string= new StringBuilder();
        for(int i=0;i<rows;i++){
            for(int j=0;j<coloumns;j++){
                string.append(getElement(i,j)).append(" ");
            }
        }
        return string.toString();
    }
   
  
    public static void main(String[] args){
        Scanner scanner=new Scanner(System.in);
        System.out.println("First matrix :");
        System.out.println("Number of rows of the matrix = ");
        int row=scanner.nextInt();
        System.out.println("Enter the number of coloumns of the matrix = ");
        int cols=scanner.nextInt();
        
        Matrix mat1=new Matrix(row,cols);
        System.out.println("Enter the elements of the matrix : ");
        for(int i=0;i<row;i++){
            for(int j=0;j<cols;j++){
                int val=scanner.nextInt();
                mat1.SetElement(i,j,val);
            }
            }
        mat1.display();
        
        System.out.println();
        System.out.println("Second matrix : ");
        System.out.println("Number of rows of matrix : ");
        int r=scanner.nextInt();
        System.out.println("Number of coloumns of matrix : ");
        int c=scanner.nextInt();
        
        Matrix mat2=new Matrix(r,c);
        System.out.println("Enter the elements of matrix : ");
        for(int i=0;i<r;i++){
            for(int j=0;j<r;j++){
                int val=scanner.nextInt();
                mat2.SetElement(i,j, val);
            }
        }
        mat2.display();
        System.out.println();
        
        System.out.println("Addition of two matrices : ");
        Matrix mat3=mat1.add(mat2);
        if(mat3!=null){
            mat3.display();
        }
        System.out.println();
        
        System.out.println("Difference of two matrices ");
        Matrix mat4=mat1.sub(mat2);
        if(mat4!=null){
            mat4.display();
        }
        System.out.println(); 
        
         System.out.println("Multiplication of two matrices : ");
         Matrix mat5=mat1.mul(mat2);
         if(mat5!=null){
             mat5.display();
         }
         System.out.println();
         
         System.out.println("Transpose of first matrix is : ");
         Matrix mat6=mat1.transpose();
         mat6.display();
         System.out.println();
         System.out.println();
         
         System.out.println("String form of the matrix is : ");
         System.out.println(mat1.toString());
         

         
    }
}

