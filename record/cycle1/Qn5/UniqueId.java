import java.util.Date;
import java.util.Scanner;

class Matrix3 {
    private int rows;
    private int col;
    private int data[][];
    
    public Matrix3(int rows, int cols, int[] Data) {
        this.rows = rows;
        this.col = cols;
        this.data = new int[rows][cols];
        int k = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                this.data[i][j] = Data[k++];
            }
        }
    }
    
    public void display(){
        for(int i=0;i<rows;i++){
            for(int j=0;j<col;j++){
                System.out.print(data[i][j]+" ");
            }
            System.out.println();
        }
    }
    
    public int column_sum(int col_no){
        if(col_no>=0 && col_no<col ){
            int col_sum=0;
            for(int i=0;i<rows;i++){
                col_sum+=data[i][col_no];
            }
            return col_sum;
        }
        else{
            System.out.println("Incorrect column number");
            return -1;
        }
    }
    
    public int row_sum(int row_no){
        if(row_no>=0 && row_no<rows){
            int row_sum=0;
            for(int i=0;i<col;i++){
                row_sum+=data[row_no][i];
            }
            return row_sum;
        }
        else{
            System.out.println("Incorrect row number");
            return -1;
        }
    }
    
    public void average (){
        double sum=0;
        for(int i=0;i<this.rows;i++){
            for(int j=0;j<this.col;j++){
                sum+=this.data[i][j];
            }
        }
        double avg=sum/(this.rows*this.col);
        System.out.println("Average of all elements of matrix = "+avg);
    }
    
    public boolean isDiagonal() {
        if (rows != col) {
            return false;
        }
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < col; j++) {
                if (i != j && data[i][j] != 0) {
                    return false;
                }
            }
        }
        return true;
    }
}

public class UniqueId {
    private static Scanner in = new Scanner(System.in);
    private static Matrix3 matx;
    private static int ObjectCount = 0;
    private static Date date;
    private static int UniqueID;
    
    public UniqueId(){
        ObjectCount++;
        date = new Date();
        UniqueID = ObjectCount;
    }
    
    public void displayData(){
        System.out.println("Date : "+date);
        System.out.println("Unique Id : "+UniqueID);
    }
    
    public static void main(String[] args){
 
        
        char choice;
        do {
            System.out.println("\nMenu:");
            System.out.println("a. Create/Update matrix");
            System.out.println("b. Print matrix");
            System.out.println("c. Print Column Sum");
            System.out.println("d. Print Row Sum");
            System.out.println("e. Print Average of matrix");
            System.out.println("f. Check if matrix is Diagonal");
            System.out.println("g. Display Unique ID and Date");
            System.out.println("h. Exit");
            System.out.print("Enter your choice: ");
            choice = in.next().charAt(0);

            switch (choice) {
                case 'a':
                    createOrUpdateMatrix();
                    break;
                case 'b':
                    printMatrix();
                    break;
                case 'c':
                    printColumnSum();
                    break;
                case 'd':
                    printRowSum();
                    break;
                case 'e':
                    printAverage();
                    break;
                case 'f':
                    checkDiagonal();
                    break;
                case 'g':
                    displayUniqueIdAndDate();
                    break;
                case 'h':
                    System.out.println("Thank you!");
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        } while (choice != 'h');
    }

    public static void createOrUpdateMatrix() {
        System.out.print("Enter number of rows: ");
        int rows = in.nextInt();
        System.out.print("Enter number of columns: ");
        int cols = in.nextInt();
        System.out.println("Enter matrix elements row-wise:");
        int[] elements = new int[rows * cols];
        for (int i = 0; i < rows * cols; i++) {
            elements[i] = in.nextInt();
        }
        matx = new Matrix3(rows, cols, elements); 
        ObjectCount++;
        date= new Date();
        UniqueID = ObjectCount;
        System.out.println("Matrix created/updated successfully.");
    }

    public static void printMatrix() {
        if (matx != null) {
            System.out.println("Matrix:");
            matx.display();
        } 
        else {
            System.out.println("No matrix exists. Please create one first.");
        }
    }

    public static void printColumnSum() {
        if (matx != null) {
            System.out.print("Enter column number: ");
            int col = in.nextInt();
            int sum = matx.column_sum(col);
            System.out.println("Sum of column " + col + ": " + sum);
        } else {
            System.out.println("No matrix exists. Please create one first.");
        }
    }

    public static void printRowSum() {
        if (matx != null) {
            System.out.print("Enter row number: ");
            int row = in.nextInt();
            int sum = matx.row_sum(row);
            System.out.println("Sum of row " + row + ": " + sum);
        } else {
            System.out.println("No matrix exists. Please create one first.");
        }
    }

    public static void printAverage() {
        if (matx != null) {
             matx.average();
        } else {
            System.out.println("No matrix exists. Please create one first.");
        }
    }

    public static void checkDiagonal() {
        if (matx != null) {
            String result = matx.isDiagonal() ? "It is a diagonal matrix" : "It is not a diagonal matrix";
            System.out.println(result);
        } else {
            System.out.println("No matrix exists. Please create one first.");
        }
    }

    public static void displayUniqueIdAndDate() {
        
        System.out.println("Date : " + date);
        System.out.println("Unique Id : " + UniqueID);
    }
}

