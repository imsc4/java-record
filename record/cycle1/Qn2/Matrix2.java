public class Matrix2 {
    private int row;
    private int col;
    private int data[][];
    public Matrix2(int r,int c,int[] element){
        this.row=r;
        this.col=c;
        this.data=new int[row][col];
        int index=0;
        for(int i=0;i<row;i++){
            for(int j=0;j<col;j++){
                this.data[i][j]=element[index];
                index++;
            }
        }
    }
    public void display(){
        System.out.println("Matrix : ");
        for(int i=0;i<row;i++){
            for(int j=0;j<col;j++){
                System.out.print(data[i][j]+" ");
            }
            System.out.println();
        }
    }
    
    public static void main(String[] args){
        try{
            int rows = Integer.parseInt(args[0]);
            int cols = Integer.parseInt(args[1]);
            int[] elements = new int[args.length-2];
            for(int i=0;i<rows*cols;i++){
                elements[i] = Integer.parseInt(args[i+2]);
            }
            Matrix2 matrix2 = new Matrix2(rows, cols, elements);
            matrix2.display();
        }
        catch(NumberFormatException e){
                System.out.println("Invalid input. Please provide integers only");
        }
        catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Insufficient arguments provided.");
        }
    }
}
