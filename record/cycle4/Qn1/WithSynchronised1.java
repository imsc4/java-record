 public class WithSynchronised1 {   
    public static void main(String[] args) {
        // Create a shared bank account
        BankAccount account = new BankAccount();

        // Create and start 100 threads, each adding 1 rupee to the account
        for (int i = 0; i < 100; i++) {
            Thread thread = new Thread(() -> {
                account.addMoney(1);
            });
            thread.start();
        }

        // Wait for all threads to finish
        try {
            Thread.sleep(2000); // wait for 2 seconds to ensure all threads have finished
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Print the final balance
        System.out.println("Final balance: " + account.getBalance());
    }
}

class BankAccount {
    private int balance;

    public BankAccount() {
        this.balance = 0;
    }

    // Synchronized method to add money to the account
    public synchronized void addMoney(int amount) {
        // Simulate some processing time
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        balance += amount;
    }

    // Method to get the current balance
    public int getBalance() {
        return balance;
    }
}

