import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class WriteFile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of lines you want to write: ");
        int n = scanner.nextInt();
        scanner.nextLine(); // consume the newline character
        
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("sentences.txt"))) {
            System.out.println("Enter " + n + " lines:");

            for (int i = 0; i < n; i++) {
                String line = scanner.nextLine();
                writer.write(line);
                writer.newLine();
            }

            System.out.println("Lines written to sentences.txt successfully.");
        } catch (IOException e) {
            System.out.println("An error occurred while writing to the file: " + e.getMessage());
        } finally {
            scanner.close();
        }
    }
}

