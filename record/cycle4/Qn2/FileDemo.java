import java.io.File;
import java.io.IOException;

public class FileDemo {
	public static void main (String[] args ){
               // Create a new file
 	File file = new File("test.txt");
 	try {
 	// Create the file (if it doesn’t exist)
 		if (file.createNewFile()) {
 		System.out.println("File created: " + 	file.getName());
 	} else {
 		System.out.println("File already exists.");
 	}
 	
 	// Get file properties
 	System.out.println("Absolute path: " + file.getAbsolutePath());
 	System.out.println("Canonical path " + file.getCanonicalPath() );
 	System.out.println("File name: " + file.getName());
 	System.out.println("File path: " + file.getPath());
 	System.out.println("Parent directory: " + file.getParent());
 	System.out.println("File exists: " + file.exists());
 	System.out.println("Is directory: " + file.isDirectory());
 	System.out.println("Is file: " + file.isFile());
 	System.out.println("File length: " + file.length() + " bytes");
 	System.out.println("can Read " + file.canRead() );
 	System.out.println("can Write " + file.canWrite() );
 	System.out.println("is Absolute " + file.isAbsolute() );
 	System.out.println("is Hidden " + file.isHidden() );
 	} catch(IOException e){
 		System.out.println("An error occurred while creating the file: " + e.getMessage());
 	}
 	// Delete the file
 	if (file.delete()) {
 		System.out.println("File deleted.");
 	} else {
 		System.out.println("Failed to delete the file.");
 	}
      }
 }
 
