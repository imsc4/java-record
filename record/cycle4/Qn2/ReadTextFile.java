 import java.io.File;
 import java.io.FileNotFoundException;
 import java.util.Scanner;
 public class ReadTextFile {
 	public static void main(String[] args) {
 	// Check if the filename is provided as a command-line argument
 	if (args.length != 1) {
 	    System.out.println("Usage: java ReadFile <filename>");
 		return;
 	}
 	
	 // Get the filename from the command-line argument
 	String filename = args[0];
 	// Try to read the file and print its content
 	try (Scanner scanner = new Scanner(new File(filename))) {
 		System.out.println("Contents of " + filename + ":");
 		while (scanner.hasNextLine()) {
 			System.out.println(scanner.nextLine());
 		}
 		} catch (FileNotFoundException e) {
 			System.out.println("File not found: " + filename);
 		}
 	}
 }
