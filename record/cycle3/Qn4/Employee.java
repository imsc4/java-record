package s2.employee;
import java.util.Scanner;

abstract class Employee {
    Scanner in = new Scanner(System.in);
    private int empid;
    private String empname;
    private double salary;

    public Employee(int id, String name, double Sal) {
        empid = id;
        empname = name;
        salary = Sal;
    }

    abstract double CalculateSalary();

    abstract void DisplayInfo();

    abstract void enterInfo();

    public int getId() {
        return empid;
    }

    public void setId(int id) {
        empid = id;
    }

    public String getName() {
        return empname;
    }

    public void setName(String name) {
        empname = name;
    }

    public double getSal() {
        return salary;
    }

    public void setSal(double sal) {
        salary = sal;
    }
}

class Manager extends Employee {
    private double allowance = 0.5;

    Manager(int id, String name, double sal) {
        super(id, name, sal);
    }

    public double CalculateSalary() {
        double Totsal = allowance * super.getSal() + super.getSal();
        return Totsal;
    }

    public void DisplayInfo() {
        System.out.println("Employee Post : Manager");
        System.out.println("Manager Id : " + super.getId());
        System.out.println("Manager Name : " + super.getName());
        double Tot = CalculateSalary();
        System.out.println("Manager Total salary " + Tot);
    }

    public void enterInfo() {
        System.out.println("Enter the Employee Id : ");
        int id = in.nextInt();
        super.setId(id);
        System.out.println("Enter the Employee Name : ");
        String Name = in.next();
        super.setName(Name);
        System.out.println("Enter Employee  Salary : ");
        double Sal = in.nextDouble();
        super.setSal(Sal);
    }
}

class Programmer extends Employee {
    private double allowance;

    Programmer(int id, String name, double sal) {
        super(id, name, sal);
    }

    public double CalculateSalary() {
        allowance = 0.7;
        double Totsal = allowance * super.getSal() + super.getSal();
        return Totsal;
    }

    public void enterInfo() {
        System.out.println("Enter the Employee Id : ");
        int id = in.nextInt();
        super.setId(id);
        System.out.println("Enter the Employee Name : ");
        String Name = in.next();
        super.setName(Name);
        System.out.println("Enter Employee  Salary : ");
        double Sal = in.nextDouble();
        super.setSal(Sal);
    }

    public void DisplayInfo() {
        System.out.println("Employee Post : Programmer");
        System.out.println("Programmer Id : " + super.getId());
        System.out.println("Programmer Name : " + super.getName());
        double Tots = CalculateSalary();
        System.out.println("Programmer Total Sal " + Tots);
    }
}

public class Main {
    public static void EmployeeList(Employee[] emp) {
        System.out.println("NAME\t\tSALARY");
        int len = emp.length;
        for (int i = 0; i < len; i++) {
            System.out.println(emp[i].getName() + "\t\t" + emp[i].CalculateSalary());
        }
    }

    public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);
        int limit;
        System.out.println("Enter the Number of employees :");
        limit = obj.nextInt();
        String choice;
        Employee[] e = new Employee[limit];
        for (int i = 0; i < limit; i++) {
            System.out.println("Enter your choice\n");
            System.out.println("M : Manager\n P: Programmer");
            choice = obj.next();
            if (choice.equals("M")) {
                Manager M1 = new Manager(0, "", 0);
                M1.enterInfo();
                e[i] = M1;
            }
            if (choice.equals("P")) {
                Programmer P1 = new Programmer(0, "", 0);
                P1.enterInfo();
                e[i] = P1;
            }
        }
        EmployeeList(e);
    }
}

