import java.util.Scanner;

class InvalidMoveException extends Exception {
    public InvalidMoveException(String message) {
        super(message);
    }
}


