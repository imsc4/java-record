import java.util.Scanner;

public class TicTacToee {
    static SquareMatrix board;
    static String turn;

    public TicTacToee() {
        board = new SquareMatrix(3);
    }

    public boolean MakeMove(int row, int col, int turn) throws InvalidMoveException {
        if (row < 0 || row >= 3 || col < 0 || col >= 3) {
            throw new InvalidMoveException("INVALID MOVE: Row and column values must be between 0 and 2.");
        } else if (board.getElement(row, col) != 0) {
            return false;
        } else {
            board.SetElement(row, col, turn);
            return true;
        }
    }

    public boolean isGameOver() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board.getElement(i, j) == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean checkWin(int row, int col, int turn) {
        int sumrow = 0;
        int sumcol = 0;
        int sumdiagonal1 = 0;
        int sumdiagonal2 = 0;

        for (int i = 0; i < 3; i++) {
            sumrow += board.getElement(row, i);
            sumcol += board.getElement(i, col);
        }

        if (row == col) {
            for (int i = 0; i < 3; i++) {
                sumdiagonal1 += board.getElement(i, i);
            }
        }
        if (row + col == 2) {
            for (int i = 0; i < 3; i++) {
                sumdiagonal2 += board.getElement(i, 2 - i);
            }
        }

        if (sumrow == 3 || sumrow == -3 || sumcol == 3 || sumcol == -3
                || sumdiagonal1 == 3 || sumdiagonal1 == -3 || sumdiagonal2 == 3 || sumdiagonal2 == -3) {
            return true;
        }
        return false;
    }

    public void printBoard() {
        System.out.println("__________________________");
        for (int i = 0; i < 3; i++) {
            System.out.print("|");
            for (int j = 0; j < 3; j++) {
                if (board.getElement(i, j) == -1) {
                    System.out.print("   X   ");
                } else if (board.getElement(i, j) == 1) {
                    System.out.print("   O   ");
                } else {
                    System.out.print("   _   ");
                }
                System.out.print("|");
            }
            System.out.println();
            System.out.println("_________________________");
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        TicTacToee game = new TicTacToee();
        turn = "X";
        boolean gameEnd = false;
        String winner = null;
        System.out.println("Welcome to 3*3 Tic Tac Toe.");
        game.printBoard();
        System.out.println("X will play first. Enter a slot number to place X in: ");

        while (!gameEnd) {
            int numInput;
            try {
                numInput = in.nextInt();
                if (!(numInput > 0 && numInput <= 9)) {
                    System.out.println("Invalid input: re-enter slot number:");
                    continue;
                }
                int row = (numInput - 1) / 3;
                int col = (numInput - 1) % 3;

                if (game.MakeMove(row, col, turn.equals("X") ? -1 : 1)) {
                    game.printBoard();
                    if (game.checkWin(row, col, turn.equals("X") ? -1 : 1)) {
                        winner = turn;
                        gameEnd = true;
                    } else if (game.isGameOver()) {
                        winner = "draw";
                        gameEnd = true;
                    } else {
                        turn = turn.equals("X") ? "O" : "X";
                        System.out.println(turn + " It's your turn! Enter a slot number where " + turn + " in: ");
                    }
                } else {
                    System.out.println("Slot already taken or invalid move: re-enter slot number:");
                }
            } catch (InvalidMoveException e) {
                System.out.println(e.getMessage());
            }
        }

        if (winner.equalsIgnoreCase("draw")) {
            System.out.println("It's a draw! Thanks for playing.");
        } else {
            System.out.println("Congratulations! " + winner + " has won! Thanks for playing.");
        }
        in.close();
    }
}
