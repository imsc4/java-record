import java.util.Scanner;
import java.util.Random;

interface Player {
    void play(TicTacToe game);
}

class HumanPlayer implements Player {
    @Override
    public void play(TicTacToe game) {
        Scanner scanner = new Scanner(System.in);
        game.printBoard();
        System.out.println("Player"+game.getCurrentPlayer()+" , enter your move :");
        int numInput = scanner.nextInt();
        int row = (numInput - 1) / 3;
        int col = (numInput - 1) % 3;
        try {
            game.MakeMove(row, col, game.turn.equals("X") ? -1 : 1);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
}

class ComputerPlayer implements Player {
    @Override
    public void play(TicTacToe game) {
        Random random = new Random();
        int row, col;
        do {
        	System.out.println("Player"+game.getCurrentPlayer()+" , enter your move : ");
            row = random.nextInt(3);
            col = random.nextInt(3);
        } while (game.board.getElement(row, col) != 0);

        try {
            game.MakeMove(row, col, game.turn.equals("O") ? -1 : 1);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
}

public class TicTacToeAI{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        TicTacToe game = new TicTacToe();
        int row = 3;
        int col = 3;
        int currentPlayer =-1;
        Player player1 = new HumanPlayer();
        Player player2 = new ComputerPlayer();
        boolean gameEnd = false;
        String winner = null;

        System.out.println("Welcome to 3*3 Tic Tac Toe.");
        game.printBoard();
        System.out.println("X will play first.");

        while (!gameEnd) {
            player1.play(game);
            game.printBoard();
            if (game.checkWin(row,col,game.turn.equals("X")? -1 : 1 ) || game.isGameOver()) {
                break;
            }

            player2.play(game);
            game.printBoard();
            if (game.checkWin(row,col,game.turn.equals("O") ? -1 : 1) || game.isGameOver()) {
                break;
            }
        }

        if (game.isGameOver()) {
            System.out.println("It's a draw! Thanks for playing.");
        } else {
            winner = game.turn.equals("X") ? "O" : "X";
            System.out.println("Congratulations! " + winner + " has won! Thanks for playing.");
        }
        scanner.close();
    }
}

