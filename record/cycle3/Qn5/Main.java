import java.util.Scanner;

// PaymentMethod interface
interface PaymentMethod {
    void pay(double amount);
    void cancel(double amount);
}

// CreditCard class implementing PaymentMethod interface
class CreditCard implements PaymentMethod {
    private String cardNumber;
    private String expiryDate;
    private String cvv;

    public CreditCard(String cardNumber, String expiryDate, String cvv) {
        this.cardNumber = cardNumber;
        this.expiryDate = expiryDate;
        this.cvv = cvv;
    }

    public void pay(double amount) {
        // Implementation of paying with credit card
        System.out.println("Paid $" + amount + " with credit card " + cardNumber);
    }

    public void cancel(double amount) {
        // Implementation of canceling payment with credit card
        System.out.println("Cancelled payment of $" + amount + " with credit card " + cardNumber);
    }
}

// PayPal class implementing PaymentMethod interface
class PayPal implements PaymentMethod {
    private String email;
    private String password;

    public PayPal(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public void pay(double amount) {
        // Implementation of paying with PayPal
        System.out.println("Paid $" + amount + " with PayPal account " + email);
    }

    public void cancel(double amount) {
        // Implementation of canceling payment with PayPal
        System.out.println("Cancelled payment of $" + amount + " with PayPal account " + email);
    }
}

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean runAgain = true;

        while (runAgain) {
            // Prompt user to choose payment method
            System.out.println("Choose payment method:");
            System.out.println("1. Credit Card");
            System.out.println("2. PayPal");
            int choice = scanner.nextInt();

            PaymentMethod paymentMethod;

            // Based on user choice, create instance of selected payment method
            if (choice == 1) {
                System.out.println("Enter Credit Card details:");
                System.out.print("Card Number: ");
                String cardNumber = scanner.next();
                System.out.print("Expiry Date: ");
                String expiryDate = scanner.next();
                System.out.print("CVV: ");
                String cvv = scanner.next();
                paymentMethod = new CreditCard(cardNumber, expiryDate, cvv);
            } else if (choice == 2) {
                System.out.println("Enter PayPal details:");
                System.out.print("Email: ");
                String email = scanner.next();
                System.out.print("Password: ");
                String password = scanner.next();
                paymentMethod = new PayPal(email, password);
            } else {
                System.out.println("Invalid choice. Exiting...");
                return;
            }

            // Prompt user to select pay or cancel
            System.out.println("Select action:");
            System.out.println("1. Pay");
            System.out.println("2. Cancel");
            int action = scanner.nextInt();

            // Perform pay or cancel based on user selection
            if (action == 1) {
                System.out.print("Enter amount to pay: ");
                double amount = scanner.nextDouble();
                paymentMethod.pay(amount);
            } else if (action == 2) {
                System.out.print("Enter amount to cancel: ");
                double amount = scanner.nextDouble();
                paymentMethod.cancel(amount);
            } else {
                System.out.println("Invalid action. Exiting...");
                return;
            }

            // Prompt user to go to next payment or exit
            System.out.println("Do you want to go to the next payment? (Y/N)");
            String nextPayment = scanner.next();
            if (!nextPayment.equalsIgnoreCase("Y")) {
                runAgain = false;
            }
        }

        scanner.close();
    }
}

